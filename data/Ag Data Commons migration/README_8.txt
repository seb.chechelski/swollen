Dataset Title: Data from: Honey bee hives decrease wild bee abundance, species richness, and fruit count on farms regardless of wildflower strips

Name and contact information of PI:
a. Name: Gina Angelella
b. Institution: USDA-ARS, Temperate Tree Fruit and Vegetable Research Unit
c. Address: 5230 Konnowac Pass Road, Wapato, WA, 98951
d. Email: Gina.Angelella@usda.gov
e. ORCiD ID: 0000-0003-2874-9470

Funding source: USDA-NIFA Agroecosystem Management Award, 2015-67019-23215

Abstract: Pollinator refuges such as wildflower strips are planted on farms with the goals of mitigating wild pollinator declines and promoting crop pollination services. It is unclear, however, whether or how these goals are impacted by managed honey bee (Apis mellifera L.) hives on farms. We examined how wildflower strips and honey bee hives and/or their interaction influence wild bee communities and the fruit count of two pollinator-dependent crops across 21 farms in the Mid-Atlantic U.S. Although wild bee species richness increased with bloom density within wildflower strips, populations did not differ significantly between farms with and without them whereas fruit counts in both crops increased on farms with wildflower strips during one of two years. By contrast, wild bee abundance decreased by 48%, species richness by 20%, and strawberry fruit count by 18% across all farm with honey bee hives regardless of wildflower strip presence, and winter squash fruit count was consistently lower on farms with wildflower strips with hives as well. This work demonstrates that honey bee hives could detrimentally affect fruit count and wild bee populations on farms, and that benefits conferred by wildflower strips might not offset these negative impacts. Keeping honey bee hives on farms with wildflower strips could reduce conservation and pollination services.

Brief description of collection and processing of data: All data were collected from 21 farms located in the Eastern Shore region of Virginia and Maryland and Virginia Beach. Ten farms had wildflower meadows sown either in 2015 (N=1 farm) or 2016(N=9 farms) which were planted following NRCS guidelines for the creation of pollinator refuges. Honey bee hive presence/absence varied by farm. Please see mansucript methods and supplemental materials for more details.
Strawberry and winter squash fruit count data: Fruits were collected from six strawberry plants and four winter squash plants, grown in two 50-gal containers at each farm. Strawberries were grown in the spring and winter squash in the summer of 2017 and 2018. All were grown in the same growing medium with consistent fertilizers and watered as needed. Strawberry plants were grown in a mesh cage to prevent vertebrate foraging. We quantified total strawberry and winter squash fruit produced per farm and year. Please see manuscript methods for more details.
Wild bees: We trapped bees during two 48 h periods each year in UV-bright yellow, blue, and white pan traps and 3 blue vane traps at each farm. The sampling periods roughly corresponded with strawberry flowering during the spring (mid-May to early June) and winter squash flowering (early August) during the summer. Bees (Hymenoptera: Apoidea) were identified to species. We calculated the abundance, species richness, evenness, and Shannon-Wiener diversity of wild bees within traps at each farm per sampling event and year.

Description of files: There are 3 CSV spreadsheets.

Definition of acronyms, codes, and abbreviations: N/A

Description or definition of any other unique information that would help others use your data: The data were collected as described in the manuscript. Data are comma-delimited.

Description of parameters/variables:
a. Temporal (beginning and end dates of data collection): 2017-2018
b. Instruments used and units of measurements: Described in the manuscript.
c. Column headings of data files (for tabular data): Within the comma-delimited files.
d. Location/GIS Coverage (if applicable to data): Described in the manuscript.
e. Symbol used for missing data: N/A

Special software required to use data: N/A

Publications that cite or use these data: Angelella, G.M., McCullough, C.T., & O'Rourke, M.E. Honey bee hives decrease wild bee abundance, species richness, and fruit count on farms regardless of wildflower strips. Scientific Reports (accepted)

These data were not derived from another data source.