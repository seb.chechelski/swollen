Welcome to - BeeRisk Assessment
====================================================
.. note::

   This project is under active development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   public/pages/research_attributes
   code/lab

Introduction
==================
Bees play a crucial role in the pollination system, significantly impacting the agricultural sector. As pollinators, bees facilitate the reproduction of flowering plants by transferring pollen from male to female flower parts. This process is essential for the production of fruits, seeds, and vegetables. In agriculture, the majority of crops rely on pollination, and bees are among the most effective pollinators. Their contribution enhances crop yields and promotes genetic diversity. The decline in bee populations poses a serious threat to global food security, as it could lead to reduced agricultural productivity. Sustainable farming practices and conservation efforts are imperative to safeguard these vital pollinators and ensure a resilient agricultural ecosystem.

.. image:: /public/assets/640px-The_Lone_Pollinator.jpg

Bee picture [Img1]_

Goal of the project
==================
Project Goal: Develop a comprehensive method to quantify risks to bee existence and pollination in agricultural ecosystems, identifying stressors like pesticides and habitat loss. Provide actionable insights for conservation and sustainable farming.


.. [Img1] By Tanner Smida - Own work, CC BY 4.0, https://commons.wikimedia.org/w/index.php?curid=63835519
